﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

public class PlayerFire : MonoBehaviour
    // Start is called before the first frame update
 {   
        // 총알 생산 공장
        public GameObject bulletFactory;

        public int poolSize = 10;

        GameObject[] bulletObjectPool;

        // 총구
        public GameObject firePosition;

    private void Start()
    {
            #if UNITY_ANDROID
                GameObject.Find("Joystick canvas XYBZ").SetActive(true);
            #elif UNITY_EDITER || UNITY_STANDALONE
                GameObject.Find("Joystick canvas XYBZ").SetActive(false);
            #endif
    }

    private void Update()
    {
#if UNITY_EDITOR || UNITY_STANDALONE
        if (Input.GetButtonDown("Fire1"))
        {
            Fire();
        }
#endif
    }
    public void Fire()
    { 

        GameObject bullet = Instantiate(bulletFactory);

            bullet.transform.position = firePosition.transform.position;
        }
    }


